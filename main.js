const express = require("express")
const app = express();
const PORT = 3000;


console.log("Server-side program starting...");

app.get('/', (req,res) => {
    res.send('Hello world');
});

//@param {Number} a
//@param {Number} b
//@returns {Number}

function add(a) {
    const sum = a + b;
    return sum;
 };

 app.get('/add', (req, res) => {
    console.log(res.query);
    const sum = add(res.query.a, req.query.b);
    res.send(sum); 
})

app.listen(PORT, () => console.log(
    `Server listening http://localhost:${PORT}`
));